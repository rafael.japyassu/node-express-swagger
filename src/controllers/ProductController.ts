import { Request, Response } from "express";
import { ProductService } from "../services/ProductService";

export class ProductController {
  constructor(private service: ProductService) {}

  async index(request: Request, response: Response) {
    const products = await this.service.findAll();

    return response.json(products);
  }

  async findById(request: Request, response: Response) {
    const { id } = request.params;
    const product = await this.service.findById(id);

    return response.json(product);
  }
  
  async create(request: Request, response: Response) {
    const { name, description, price } = request.body;
    const product = await this.service.create({
      name,
      description,
      price
    });

    return response.status(201).json(product);
  }
}