import express from 'express';
import { appRoutes } from './routes';
import swaggerUI from 'swagger-ui-express';
import YAML from 'yamljs';
import { resolve } from 'path';
const swaggerDocument = YAML.load(resolve(__dirname, './docs/swagger.yaml'));

const app = express();
const port = 3000;

app.use(express.json());
app.get('/', (request, response) => {
  return response.json({ message: 'OK' });
});
app.use(appRoutes);

app.use('/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.listen(port, () => {
  console.log(`API rodando no... http://localhost:${port}`);
});