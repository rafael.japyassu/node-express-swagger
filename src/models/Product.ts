export class Product {
  id: string;
  name: string;
  description: string;
  price: number;
  available: boolean;

  constructor(
    id: string,
    name: string,
    description: string,
    price: number,
    available: boolean,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.available = available;
  }
}