import { Product } from "../models/Product";
import { CreateProductDTO } from "../types/CreateProductDTO";

export interface IProductRepository {
  findAll(): Promise<Product[]>;
  findById(id: string): Promise<Product | undefined>;
  create(data: CreateProductDTO): Promise<Product | undefined>;
  update(product: Product): Promise<Product>;
  delete(id: string): Promise<void>;
}