import { Router } from "express";
import { productRoutes } from "./products";

const appRoutes = Router();

appRoutes.use('/products', productRoutes);

export { appRoutes };