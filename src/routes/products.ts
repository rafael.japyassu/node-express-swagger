import { Router } from "express";
import { ProductController } from "../controllers/ProductController";
import { ProductService } from "../services/ProductService";
import { ProductRepositoryInMemory } from "../utils/ProductRepositoryInMemory";

const productRoutes = Router();
const repository = new ProductRepositoryInMemory();
const service = new ProductService(repository);
const controller = new ProductController(service);

productRoutes.get('/', controller.index.bind(controller));
productRoutes.post('/', controller.create.bind(controller));
productRoutes.get('/:id', controller.findById.bind(controller));

export { productRoutes };