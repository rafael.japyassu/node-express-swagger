import { IProductRepository } from "../repositories/ProductRepository";
import { CreateProductDTO } from "../types/CreateProductDTO";
import { UpdateProductDTO } from "../types/UpdateProductDTO";

export class ProductService {
  constructor(private productRepository: IProductRepository) {}

  async findAll() {
    return this.productRepository.findAll();
  }

  async findById(id: string) {
    return this.productRepository.findById(id);
  }

  async create(data: CreateProductDTO) {
    const product = await this.productRepository.create(data);
    return product;
  }

  async update(id: string, data: UpdateProductDTO) {
    const product = await this.productRepository.findById(id);

    if (!product) {
      throw new Error('Product not found');
    }

    product.name = data.name;
    product.description = data.description;
    product.price = data.price;
    product.available = data.available;

    await this.productRepository.update(product);
    return product;
  }

  async delete(id: string) {
    const product = await this.productRepository.findById(id);

    if (!product) {
      throw new Error('Product not found');
    }

    await this.productRepository.delete(id);
  }

}