export type CreateProductDTO = {
  name: string;
  description: string;
  price: number;
}