export type UpdateProductDTO = {
  name: string;
  description: string;
  price: number;
  available: boolean;
}