import { randomUUID } from 'crypto';
import { Product } from "../models/Product";
import { IProductRepository } from "../repositories/ProductRepository";
import { CreateProductDTO } from "../types/CreateProductDTO";

export class ProductRepositoryInMemory implements IProductRepository {
  private products: Product[];

  constructor() {
    this.products = [];
  }

  async findAll(): Promise<Product[]> {
    return this.products;
  }

  async findById(id: string): Promise<Product | undefined> {
    return this.products.find(product => product.id === id);
  }

  async create({
    description,
    name,
    price
  }: CreateProductDTO): Promise<Product | undefined> {
    const id = randomUUID();
    const product = new Product(
      id,
      name,
      description,
      price,
      true
    );

    this.products.push(product);

    return product;
  }

  async update(product: Product): Promise<Product> {
    const index = this.products.findIndex(item => item.id === product.id);

    this.products[index] = product;

    return product;
  }

  async delete(id: string): Promise<void> {
    const index = this.products.findIndex(product => product.id === id);

    this.products.splice(index, 1);
  }

}